package connect.hibernateDB;

import jdk.jfr.events.ExceptionThrownEvent;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.omg.CORBA.UserException;

import java.util.*;

public class TestDB {
    SessionFactory factory = null;
    Session session = null;

    private static TestDB single_instance =  null;

    private TestDB(){
        factory = HibernateUtils.getSessionFactory();
    }

    public static TestDB getInstance(){
        if (single_instance == null){
            single_instance = new TestDB();
        }return single_instance;
    }

    public List<CarInfo> getCarInfoAll(){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from connect.hibernateDB.CarInfo";
            List<CarInfo> cs = (List<CarInfo>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public CarInfo getCarInfo(int idcarInfo){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from connect.hibernateDB.CarInfo where idcarInfo=" + Integer.toString(idcarInfo);
            CarInfo c = (CarInfo)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }

}
