package connect.hibernateDB;


import javax.persistence.*;

@Entity
@Table(name="carinfo")
public class CarInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcarInfo")
    private int idcarInfo;

    @Column(name = "carInfoName")
    private String carInfoName;

    @Column(name = "carInfoModel")
    private String carInfoModel;

    public int getIdcarInfo() {
        return idcarInfo;
    }

    public void setIdcarInfo(int idcarInfo) {
        this.idcarInfo = idcarInfo;
    }

    public String getCarInfoName() {
        return carInfoName;
    }

    public void setCarInfoName(String carInfoName) {
        this.carInfoName = carInfoName;
    }

    public String getCarInfoModel() {
        return carInfoModel;
    }

    public void setCarInfoModel(String carInfoModel) {
        this.carInfoModel = carInfoModel;
    }

    public String toString(){
        return Integer.toString(idcarInfo) + " " + carInfoName + " " + carInfoModel;
    }

}
