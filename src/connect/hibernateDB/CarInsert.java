package connect.hibernateDB;

import org.hibernate.SessionFactory;
import org.hibernate.Session;

import java.util.Scanner;

public class CarInsert {
    public static void CarInserting(){
        TestDB insertCar = TestDB.getInstance();
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        System.out.println("Enter name and make for a car");

        Scanner input = new Scanner(System.in);
        System.out.println("name: ");
        String name = input.nextLine();
        System.out.println("make: ");
        String make = input.nextLine();

        CarInfo newCar = new CarInfo();
        newCar.setCarInfoName(name);
        newCar.setCarInfoModel(make);

        session.save(newCar);

        session.getTransaction().commit();
        HibernateUtils.shutdown();

    }
}
